#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

#define fallingEdge(x) (x == LOW && last##x == HIGH)
#define risingEdge(x) (x == HIGH && last##x == LOW)
 
ESP8266WebServer server(80);

void increase();
void decrease();
void toggleSwitch();

uint8_t filterA = 0x00;
uint8_t filterB = 0x00;
uint8_t filterSW = 0xFF;

const auto pinA = D3;
const auto pinB = D4;
const auto pinSW = D5;
const auto pinLED = D0;

int lastA = 0, lastB = 0, A = 0, B = 0, lastSW = 1, SW = 1;
int value = 0;


void sendBackLightValue() {
  server.send(200, "application/json", String("{\"value\" : ") + value + String("}"));
}
 
void setup() {
  pinMode(pinLED, OUTPUT);
  pinMode(pinA, INPUT_PULLUP);
  pinMode(pinB, INPUT_PULLUP);
  pinMode(pinSW, INPUT_PULLUP);
  Serial.begin(9600);
  WiFi.begin("Urbanovics", "K1tudja?");  //Connect to the WiFi network
 
  while (WiFi.status() != WL_CONNECTED) {  //Wait for connection
    delay(500);
    Serial.println("Waiting to connect…");
  }
 
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());  //Print the local IP
 
  server.on("/increase", []() {
    increase();
    sendBackLightValue();
  });

  server.on("/decrease", []() {
    decrease();
    sendBackLightValue();
  });

  server.on("/switch", []() {
    toggleSwitch();
    sendBackLightValue();
  });

  server.on("/change", []() {
    for(int i = 0; i < server.args(); i++) {
      if(server.argName(i) == "value")
        value = server.arg(i).toInt();
    }
    sendBackLightValue();
  });

  server.on("/", sendBackLightValue);
  server.begin();
  Serial.println("Server listening");
}

int bitSum(uint8_t input) {
  int count;
  for(count = 0; input != 0; count++)
    input &= (input - 1);
  return count;
}

int filteredRead(uint8_t port, uint8_t* filter) {
  *filter <<= 1;
  *filter |= digitalRead(port);
  return bitSum(*filter) > 0.5;
}

void loop() {
  server.handleClient();
  A = filteredRead(pinA, &filterA);
  B = filteredRead(pinB, &filterB);
  SW = filteredRead(pinSW, &filterSW);

  if(fallingEdge(A) && B == LOW)
    increase();
  
  if(fallingEdge(A) && B == HIGH)
    decrease();

  if(risingEdge(SW))
    toggleSwitch();

  analogWrite(pinLED, 1023 - value);

  lastA = A;
  lastB = B;
  lastSW = SW;
}

void increase() {
  value+=20;
  value = min(value, 1023);
  Serial.println(value);
}

void decrease() {
  value-=20;
  value = max(value, 0);
  Serial.println(value);
}

void toggleSwitch() {
  value = value <= 0 ? 1023 : 0;
  Serial.println("switch");
}

